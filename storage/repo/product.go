package repo

import "time"

type Product struct {
	ID          int64
	Name        string
	Sku         string
	Description string
	Price       float64
	Count       int64
	CreatedAt   time.Time
}

type GetAllProductsParams struct {
	Limit  int32
	Page   int32
	Search string
}

type GetAllProductsResult struct {
	Products []*Product
	Count    int32
}

type ProductStorageI interface {
	SaveProducts(p *[]Product) error
	GetAllProducts(params *GetAllProductsParams) (*GetAllProductsResult, error)
	GetProductBySku(sku string) (*Product, error)
}
