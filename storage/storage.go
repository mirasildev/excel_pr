package storage

import (
	"github.com/jmoiron/sqlx"
	"gitlab.com/mirasildev/excel_pr/storage/postgres"
	"gitlab.com/mirasildev/excel_pr/storage/repo"
)

type StorageI interface {
	Product() repo.ProductStorageI
}

type storagePg struct {
	productRepo repo.ProductStorageI
}

func NewStoragePg(db *sqlx.DB) StorageI {
	return &storagePg{
		productRepo: postgres.NewProduct(db),
	}
}

func (s *storagePg) Product() repo.ProductStorageI {
	return s.productRepo
}
