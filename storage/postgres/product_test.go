package postgres_test

import (
	"testing"

	"github.com/bxcodec/faker/v4"
	"github.com/stretchr/testify/require"
	"gitlab.com/mirasildev/excel_pr/storage/repo"
)

func TestCreateProducts(t *testing.T) {
	err := strg.Product().SaveProducts(&[]repo.Product{
		{
			Name:        faker.Word(),
			Sku:         "TS1",
			Description: faker.Word(),
			Price:       10,
			Count:       1,
		},
	})
	require.NoError(t, err)
}

func TestGetBySku(t *testing.T) {
	res, err := strg.Product().GetProductBySku("TS1")
	require.NoError(t, err)
	require.NotEmpty(t, res)
}

func TestGetAllProducts(t *testing.T) {
	res, err := strg.Product().GetAllProducts(&repo.GetAllProductsParams{
		Limit:  10,
		Page:   1,
		Search: "TS",
	})
	require.NoError(t, err)
	require.NotEmpty(t, res)
}
