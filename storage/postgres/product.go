package postgres

import (
	"database/sql"
	"errors"
	"fmt"
	"log"

	"github.com/jmoiron/sqlx"
	"gitlab.com/mirasildev/excel_pr/storage/repo"
)

type productRepo struct {
	db *sqlx.DB
}

func NewProduct(db *sqlx.DB) repo.ProductStorageI {
	return &productRepo{
		db: db,
	}
}

func (pr *productRepo) SaveProducts(pros *[]repo.Product) error {

	for _, product := range *pros {
		result, err := pr.GetProductBySku(product.Sku)
		log.Println(err)
		if errors.Is(err, sql.ErrNoRows) {
			query := `
				INSERT INTO products (
					name,
					sku,
					description,
					price,
					count
				) VALUES($1, $2, $3, $4, $5)
			`

			res, err := pr.db.Exec(
				query,
				product.Name,
				product.Sku,
				product.Description,
				product.Price,
				product.Count,
			)
			if err != nil {
				return err
			}

			rowsCount, err := res.RowsAffected()
			if err != nil {
				return err
			}

			if rowsCount == 0 {
				return err
			}

		} else {
			queryUpdate := `
				UPDATE products SET
					count=$1,
					name=$2,
					description=$3,
					price=$4
				WHERE sku=$5
			`
			res, err := pr.db.Exec(
				queryUpdate, 
				product.Count+result.Count, 
				product.Name,
				product.Description,
				product.Price,
				product.Sku,
			)
			if err != nil {
				return err
			}

			rowsCount, err := res.RowsAffected()
			if err != nil {
				return err
			}

			if rowsCount == 0 {
				return err
			}
		}
	}
	return nil
}

func (pr *productRepo) GetAllProducts(params *repo.GetAllProductsParams) (*repo.GetAllProductsResult, error) {
	result := repo.GetAllProductsResult{
		Products: make([]*repo.Product, 0),
	}

	offset := (params.Page - 1) * params.Limit

	limit := fmt.Sprintf(" LIMIT %d OFFSET %d ", params.Limit, offset)

	filter := ""
	if params.Search != "" {
		filter += " WHERE name ilike '%" + params.Search + "%' OR sku ilike '%" + params.Search + "%' "
	}

	query := `
		SELECT
			id,
			name,
			sku,
			description,
			price,
			count,
			created_at
		FROM products 
		` + filter + `
		ORDER BY created_at desc` + limit
	log.Println(query)
	rows, err := pr.db.Query(query)
	if err != nil {
		return nil, err
	}

	defer rows.Close()

	for rows.Next() {
		var (
			p repo.Product
		)

		err := rows.Scan(
			&p.ID,
			&p.Name,
			&p.Sku,
			&p.Description,
			&p.Price,
			&p.Count,
			&p.CreatedAt,
		)
		if err != nil {
			return nil, err
		}
		result.Products = append(result.Products, &p)
	}

	queryCount := "SELECT count(1) FROM products " + filter

	err = pr.db.QueryRow(queryCount).Scan(&result.Count)
	if err != nil {
		return nil, err
	}

	return &result, nil
}

func (pr *productRepo) GetProductBySku(sku string) (*repo.Product, error) {
	var result repo.Product

	query := `
		SELECT
			id,
			name,
			sku,
			description,
			price,
			count
		FROM products
		WHERE sku = $1
	`

	err := pr.db.QueryRow(query, sku).Scan(
		&result.ID,
		&result.Name,
		&result.Sku,
		&result.Description,
		&result.Price,
		&result.Count,
	)
	if err != nil {
		return nil, err
	}

	return &result, nil
}
