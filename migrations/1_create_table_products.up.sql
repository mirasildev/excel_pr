CREATE TABLE IF NOT EXISTS "products" (
    "id" SERIAL PRIMARY KEY,
    "name" VARCHAR(30),
    "sku" VARCHAR,
    "description" VARCHAR,
    "price" DECIMAL(8,2),
    "count" INTEGER,
    "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);