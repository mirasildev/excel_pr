package api

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"

	v1 "gitlab.com/mirasildev/excel_pr/api/v1"
	"gitlab.com/mirasildev/excel_pr/config"
	"gitlab.com/mirasildev/excel_pr/storage"

	swaggerFiles "github.com/swaggo/files"      // swagger embed files
	ginSwagger "github.com/swaggo/gin-swagger"  // gin-swagger middleware
	_ "gitlab.com/mirasildev/excel_pr/api/docs" // for swagger
)

type RouterOptions struct {
	Cfg     *config.Config
	Storage storage.StorageI
	Logger  *logrus.Logger
}

// / @title           Swagger for Excel file data saver
// @version         1.0
// @description     This is Excel file data saver
// @BasePath  /v1
// @securityDefinitions.apikey ApiKeyAuth
// @in header
// @name Authorization
// @Security ApiKeyAuth
func New(opt *RouterOptions) *gin.Engine {
	router := gin.Default()

	handlerV1 := v1.New(&v1.HandlerV1Options{
		Cfg:     opt.Cfg,
		Storage: opt.Storage,
		Logger:  opt.Logger,
	})

	router.Static("/sheets", "./sheets")

	apiV1 := router.Group("/v1")

	apiV1.POST("/upload-and-save", handlerV1.SaveProducts)
	apiV1.GET("/products", handlerV1.GetAllProducts)

	router.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))

	return router
}
