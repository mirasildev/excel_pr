package models

import "time"

type Product struct {
	ID          int64     `json:"id"`
	Name        string    `json:"name" binding:"required,min=2,max=50"`
	Sku         string    `json:"sku" binding:"required"`
	Description string    `json:"description"`
	Price       float64   `json:"price"`
	Count       int64     `json:"count"`
	CreatedAt   time.Time `json:"created_at"`
}

type CreateProductRequest struct {
	Name        string  `json:"name" binding:"required,min=2,max=50"`
	Sku         string  `json:"sku" binding:"required"`
	Description string  `json:"description"`
	Price       float64 `json:"price"`
	Count       int64   `json:"count"`
}

type GetAllProductsParams struct {
	Limit  int32  `json:"limit" binding:"required" default:"10"`
	Page   int32  `json:"page" binding:"required" default:"1"`
	Search string `json:"search"`
}

type GetAllProductsResponse struct {
	Products []*Product `json:"products"`
	Count    int32      `json:"count"`
}
