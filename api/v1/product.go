package v1

import (
	"log"
	"mime/multipart"
	"net/http"
	"os"
	"path/filepath"
	"strconv"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	excel "github.com/xuri/excelize/v2"
	"gitlab.com/mirasildev/excel_pr/api/models"
	"gitlab.com/mirasildev/excel_pr/storage/repo"
)

type File struct {
	File *multipart.FileHeader `form:"file" binding:"required"`
}

// @Router /upload-and-save [post]
// @Summary Upload the products and save them
// @Description Upload the Excel file with products and save them
// @Tags data
// @Accept json
// @Produce json
// @Param file formData file true "File"
// @Success 200 {object} models.ResponseOK
// @Failure 500 {object} models.ErrorResponse
func (h *handlerV1) SaveProducts(c *gin.Context) {
	var file File

	err := c.ShouldBind(&file)
	if err != nil {
		c.JSON(http.StatusBadRequest, errorResponse(err))
		return
	}

	id := uuid.New()
	fileName := id.String() + filepath.Ext(file.File.Filename)
	if filepath.Ext(file.File.Filename) != ".xlsx" {
		c.JSON(http.StatusMethodNotAllowed, errorResponse(ErrInvalidFile))
	}
	dst, _ := os.Getwd()

	if _, err := os.Stat(dst + "/sheets"); os.IsNotExist(err) {
		os.Mkdir(dst+"/sheets", os.ModePerm)
	}

	filePath := "/sheets/" + fileName
	err = c.SaveUploadedFile(file.File, dst+filePath)
	if err != nil {
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	f, err := excel.OpenFile(dst + filePath)
	if err != nil {
		h.logger.WithError(err).Error("failed to open file")
		c.JSON(http.StatusInternalServerError, err.Error())
		return
	}
	defer func() {
		if err := f.Close(); err != nil {
			c.JSON(http.StatusInternalServerError, errorResponse(err))
			return
		}
	}()

	var rows [][]string
	sheets := f.GetSheetList()
	for _, sheet := range sheets {
		sh, err := f.GetRows(sheet)
		for _, sh1 := range sh {
			rows = append(rows, sh1)
			if err != nil {
				c.JSON(http.StatusInternalServerError, errorResponse(err))
				return
			}
		}
	}

	res := make([]repo.Product, 0)
	var pr repo.Product
	
	for _, row := range rows[1:] {

		price, err := strconv.ParseFloat(row[3], 64)
		if err != nil {
			c.JSON(http.StatusInternalServerError, errorResponse(err))
			return
		}
		
		count, err := strconv.ParseInt(row[4], 10, 64)
		if err != nil {
			c.JSON(http.StatusInternalServerError, errorResponse(err))
			return
		}
		log.Println("HERE!!")
		pr = repo.Product{
			Name:        row[0],
			Sku:         row[1],
			Description: row[2],
			Price:       price,
			Count:       count,
		}

		res = append(res, pr)
	}

	err = h.storage.Product().SaveProducts(&res)
	if err != nil {
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	c.JSON(http.StatusCreated, gin.H{
		"message": "Successfully created or updated!",
	})
}

// @Router /products [get]
// @Summary Get all products
// @Description Get all products
// @Tags data
// @Accept json
// @Produce json
// @Param filter query models.GetAllProductsParams false "Filter"
// @Success 200 {object} models.GetAllProductsParams
// @Failure 500 {object} models.ErrorResponse
func (h *handlerV1) GetAllProducts(c *gin.Context) {
	req, err := validateGetAllParams(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, errorResponse(err))
		return
	}

	result, err := h.storage.Product().GetAllProducts(&repo.GetAllProductsParams{
		Page:   req.Page,
		Limit:  req.Limit,
		Search: req.Search,
	})
	if err != nil {
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	c.JSON(http.StatusOK, getProductsResponse(result))
}

func getProductsResponse(data *repo.GetAllProductsResult) *models.GetAllProductsResponse {
	response := models.GetAllProductsResponse{
		Products: make([]*models.Product, 0),
		Count:    data.Count,
	}

	for _, pro := range data.Products {
		p := parseProductModel(pro)
		response.Products = append(response.Products, &p)
	}

	return &response
}

func parseProductModel(pro *repo.Product) models.Product {
	return models.Product{
		ID:          pro.ID,
		Name:        pro.Name,
		Sku:         pro.Sku,
		Description: pro.Description,
		Price:       pro.Price,
		Count:       pro.Count,
		CreatedAt:   pro.CreatedAt,
	}
}
