package v1

import (
	"errors"
	"strconv"

	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"gitlab.com/mirasildev/excel_pr/api/models"
	"gitlab.com/mirasildev/excel_pr/config"
	"gitlab.com/mirasildev/excel_pr/storage"
)

var (
	ErrInvalidFile = errors.New("invalid file, file should be in Excel spreadsheet format")
	ErrForbidden   = errors.New("forbidden")
)

type handlerV1 struct {
	cfg     *config.Config
	storage storage.StorageI
	logger  *logrus.Logger
}

type HandlerV1Options struct {
	Cfg     *config.Config
	Storage storage.StorageI
	Logger  *logrus.Logger
}

func New(options *HandlerV1Options) *handlerV1 {
	return &handlerV1{
		cfg:     options.Cfg,
		storage: options.Storage,
		logger:  options.Logger,
	}
}

func errorResponse(err error) *models.ErrorResponse {
	return &models.ErrorResponse{
		Error: err.Error(),
	}
}

func validateGetAllParams(c *gin.Context) (*models.GetAllParams, error) {
	var (
		limit int = 10
		page  int = 1
		err   error
	)

	if c.Query("limit") != "" {
		limit, err = strconv.Atoi(c.Query("limit"))
		if err != nil {
			return nil, err
		}
	}

	if c.Query("page") != "" {
		page, err = strconv.Atoi(c.Query("page"))
		if err != nil {
			return nil, err
		}
	}

	return &models.GetAllParams{
		Limit:  int32(limit),
		Page:   int32(page),
		Search: c.Query("search"),
	}, nil
}
