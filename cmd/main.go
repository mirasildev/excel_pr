package main

import (
	"fmt"
	"log"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"gitlab.com/mirasildev/excel_pr/api"
	"gitlab.com/mirasildev/excel_pr/config"
	"gitlab.com/mirasildev/excel_pr/pkg/logger"
	"gitlab.com/mirasildev/excel_pr/storage"
)

func main() {
	cfg := config.Load(".")

	psqlUrl := fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=disable",
		cfg.Postgres.Host,
		cfg.Postgres.Port,
		cfg.Postgres.User,
		cfg.Postgres.Password,
		cfg.Postgres.Database,
	)

	psqlConn, err := sqlx.Connect("postgres", psqlUrl)

	fmt.Println(cfg)
	if err != nil {
		log.Fatalf("failed to connect database: %v", err)
	}

	strg := storage.NewStoragePg(psqlConn)

	logrus := logger.New()

	apiServer := api.New(&api.RouterOptions{
		Cfg:     &cfg,
		Storage: strg,
		Logger:  logrus,
	})

	err = apiServer.Run(cfg.HttpPort)
	if err != nil {
		log.Fatalf("failed to run server: %v", err)
	}

	log.Print("Server stopped")
}
